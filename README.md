```
Title
```
## Travel-Project

### Description

The get request of the project in the postman gives the id,destination,distance,mode and the budget of the chosen travel location

The post request allows admin user to post new field in the database in the json format

The specific get request can also be done by giving specific id

### Prerequisites

```
python==3.0 and above support the project
django==2.2.4
rest_framework==3.10.2
```
### Setting_up_project
```
Create a new Django project ,then start a new app 

#Create the project directory

make directory <dir name>

cd <dir name>

#Create a virtual environment to isolate our package dependencies locally

#on windows

python -n venv env

source env/Scripts/activate  

```
### Installing
```
Install Django and Django REST framework into the virtual environment

pip install django

pip install djangorestframework



```
### Set up a new project with a single application
```
django-admin startproject <project name> 

cd <project name>

django-admin startapp <app name>

cd..

### syncing_the_database_for_first_time
```
```bash
python manage.py makemigrations
```

```
python manage.py migrate
```

### create an initial user name with a password

```
python manage.py createsuperuser
```

This will ask for username,email and the password and the superuser will be created


### Running_the_tests

```bash
python manage.py runserver
```

This will start the server at default localhost:127.0.0.1:8000

Copy the local host number and paste it in the web. 
It will take you to the django-admin page where you can see all the model details




 